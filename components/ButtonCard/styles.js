import { StyleSheet, Image } from 'react-native';
const COLORS = require ('../../lib/colors.js')
import { verticalScale, scale, moderateScale, dimWidth } from '../../lib/scaling';

var cardSize = dimWidth / 2 - 2.5;
var iconWidth = scale(39);
var iconHeight = scale(35);

const ButtonCardStyles = StyleSheet.create({
  text: {
    color: COLORS.CLOUDS,
    fontFamily: 'open-sans-bold',
    fontSize: scale(20),
    textAlign: 'center',
    width: cardSize - scale(5),
    marginBottom: -scale(3)
  },
  icon: {
    width: iconWidth,
    height: iconHeight,
    margin: 10,
    margin: 7,
  },
  imageBackground: {
    marginTop: 0,
    marginBottom: 5,
    overflow: 'hidden',
    height: cardSize,
    width: cardSize,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

module.exports = ButtonCardStyles;