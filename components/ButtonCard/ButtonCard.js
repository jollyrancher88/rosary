import React, { Component } from 'react';
import { Text, View, ImageBackground, Image } from 'react-native';
const COLORS = require ('../../lib/colors.js');

class ButtonCard extends Component {
  textOne(media_type) {
    if (media_type == 'audio') {
      return 'LISTEN';
    } else {
      return 'PLAY';
    }
  }

  textTwo(media_type) {
    if (media_type == 'audio') {
      return 'TO AUDIO';
    } else {
      return 'VIDEO';
    }
  }

  image_src(media_type) {
    if (media_type == 'audio') {
      return require('../../assets/icons/AudioIcon.png');
    } else {
      return require('../../assets/icons/PlayVideoIcon.png');
    }
  }

  render () {
    var styles = require('./styles.js');
    var type = this.props.type || 'audio';
    var textOne = this.textOne(type);
    var textTwo = this.textTwo(type);
    var image_src = this.image_src(type);
    return (
      <ImageBackground style={styles.imageBackground} source={this.props.image}>
          <Text style={styles.text}>{textOne}</Text>
          <Text style={styles.text}>{textTwo}</Text>
          <Image
            source={image_src}
            style={styles.icon}
          />
      </ImageBackground>
    )
  }
}

module.exports = ButtonCard;