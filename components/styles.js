import { StyleSheet } from 'react-native';
const COLORS = require ('../lib/colors.js')

const AppStyles = StyleSheet.create({
  scollView: {
    padding: 0,
    paddingTop: 0,
    margin: 0,
    paddingBottom: 0,
    paddingTop: 0,
    alignSelf: 'stretch',
  },
});

module.exports = AppStyles;