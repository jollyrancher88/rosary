import { StyleSheet } from 'react-native';
const COLORS = require ('../lib/colors.js');
export default StyleSheet.create({
  container: {
    backgroundColor: COLORS.DARK_GREY,
    flex: 1
  },
  sectionHeadingStyle: {
    paddingTop: 16,
    color: COLORS.CLOUDS,
    paddingBottom: 15,
    fontFamily: 'open-sans-extrabold',
    fontSize: 18,
    paddingRight: 15,
  },
  menuIcon: {
    paddingTop: 16,
    color: COLORS.CLOUDS,
    paddingBottom: 15,
    fontSize: 18,
    paddingRight: 15,
    paddingLeft: 15,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: 'grey',
    shadowOffset: { height: 0, width: 0  }
  },
  navItemStyle: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
    color: COLORS.CLOUDS,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: 'grey',
    shadowOffset: { height: 0, width: 0 },
    padding: 20,
    paddingLeft: 25
  },
  navSectionStyle: {
    backgroundColor: COLORS.DARK_GREY,
  },
  footerContainer: {
    padding: 5,
    paddingBottom: 10,
    flexDirection: 'row',
    backgroundColor: COLORS.DARK_GREY
  },
  footerImage: {
    marginHorizontal: -10,
    width: 120,
    height: 120,
  },
  footerTextContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  footerText: {
    color: COLORS.CLOUDS,
    fontSize: 8,
    fontFamily: 'open-sans-bold',
    paddingBottom: 2
  }
});