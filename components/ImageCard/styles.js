import { StyleSheet } from 'react-native';
const COLORS = require ('../../lib/colors.js')
import { scale, moderateScale } from '../../lib/scaling';

const ImageCardStyles = StyleSheet.create({
  button: {
    padding: 0,
    borderRadius: 3,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {width: 1, height: 1},
    // backgroundColor: 'grey'
  },
  preText: {
    color: COLORS.CLOUDS,
    fontSize: scale(20)
  },
  mysteryText: {
    fontFamily: 'open-sans-extrabold',
    color: COLORS.CLOUDS,
    fontSize: scale(50),
    alignSelf: 'center'
  },
  postText: {
    color: COLORS.CLOUDS,
    fontSize: scale(30)
  },
  card: {
    marginTop: 45,
    paddingTop: 15,
    height: 30,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  imageBackground: {
    marginTop: 0,
    marginBottom: 5,
    overflow: 'hidden',
    alignItems: 'center',
    paddingTop: scale(145),
    height: scale(350),

  },
});

module.exports = ImageCardStyles;