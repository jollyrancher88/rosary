import { StyleSheet } from 'react-native';
import { scale } from '../../lib/scaling';
const COLORS = require ('../../lib/colors.js');

const HeaderStyles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
    // backgroundColor: 'red',
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 0,
  },
  menuIconContainer: {
    padding: 200,
    marginRight: 100,
    backgroundColor: 'blue',
  },
  menuIcon: {
    width: scale(25),
    height: scale(25),
    margin: 10,
  },
  spacerOne: {
    width: '50%',
    marginLeft: -(2*(scale(25)) + 20),
  },
  spacerTwo: {
    width: '50%',
    marginLeft: -(2*(scale(25))),
  },
  logoIcon: {
    width: scale(20),
    height: scale(30),
    margin: 10,
    margin: 7,
  },
  settingsIcon: {
    width: scale(25),
    height: scale(25),
    margin: 10,
  }
});

module.exports = HeaderStyles;
