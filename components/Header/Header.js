import React, { Component } from 'react';
import Expo from 'expo';
import { StatusBar, TouchableWithoutFeedback, View, Image, Text } from 'react-native'
const COLORS = require ('../../lib/colors.js');
import { scale } from '../../lib/scaling';

class Header extends Component {
  render () {
    var styles = require('./styles.js');
    var title = this.props.title || 'TITLE';
    return (
        <View style={styles.container}>
          <StatusBar hidden={true} />
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('DrawerOpen') } >
            <Image
              source={require('../../assets/icons/BurgerMenuIcon.png')}
              style={styles.menuIcon}
            />
          </TouchableWithoutFeedback>
          <View style={styles.spacerOne} />
          <Image
            source={require('../../assets/icons/Outline-of-Shell-White.png')}
            style={styles.logoIcon}
            />
          <View style={styles.spacerTwo} />
          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('DrawerOpen') } >
            <Image
              source={require('../../assets/icons/SettingsIcon.png')}
              style={styles.settingsIcon}
            />
          </TouchableWithoutFeedback>
        </View>
    )
  }
}

module.exports = Header;