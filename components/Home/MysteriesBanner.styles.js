import { StyleSheet } from 'react-native';
const COLORS = require ('../../lib/colors.js')
import { scale, moderateScale } from '../../lib/scaling';

const ImageCardStyles = StyleSheet.create({
  textContainer: {
    padding: 0,
    marginBottom: 0,
    borderRadius: 3,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {width: 1, height: 1},
  },
  textContainerLower: {
    marginTop: -10,
    padding: 0,
    borderRadius: 3,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {width: 1, height: 1},
  },
  preText: {
    color: COLORS.CLOUDS,
    fontSize: scale(20)
  },
  mysteryText: {
    marginTop: -scale(15),
    // marginBottom: -scale(12),
    fontFamily: 'open-sans-extrabold',
    color: COLORS.CLOUDS,
    fontSize: scale(45),
    alignSelf: 'center'
  },
  postText: {
    color: COLORS.CLOUDS,
    fontSize: scale(30)
  },
  imageBackground: {
    marginTop: 0,
    marginBottom: 5,
    overflow: 'hidden',
    alignItems: 'center',
    paddingTop: scale(70),
    height: scale(245),
  },
});

module.exports = ImageCardStyles;