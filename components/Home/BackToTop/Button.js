import React, { Component } from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
const COLORS = require ('../../../lib/colors.js');

class ButtonCard extends Component {

  render () {
    var styles = require('./styles.js');
    var presHandler = this.props.onPress || null;
    return (
      <TouchableHighlight onPress={presHandler} >
        <View style={styles.container}>
          <Text style={styles.text}>BACK TO TOP</Text>
        </View>
      </TouchableHighlight>
    )
  }
}

module.exports = ButtonCard;