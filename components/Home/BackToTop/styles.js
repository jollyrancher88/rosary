import { StyleSheet, Image } from 'react-native';
const COLORS = require ('../../../lib/colors.js')
import { verticalScale, scale, moderateScale, dimWidth } from '../../../lib/scaling';

const ButtonStyles = StyleSheet.create({
  container: {
    height: scale(75),
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'open-sans-bold',
    fontSize: scale(28),
    textAlign: 'center',
    marginBottom: scale(3)
  }
});

module.exports = ButtonStyles;