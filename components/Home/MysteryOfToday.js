import React, { Component } from 'react';
import { Text, View, ImageBackground } from 'react-native';
const COLORS = require ('../../lib/colors.js');

class MysteryOfToday extends Component {
  render () {
    var styles = require('./MysteryOfToday.styles.js');
    return (
      <ImageBackground style={styles.imageBackground} source={this.props.image}>
        <View style={styles.button}>
          <Text style={styles.preText}>MYSTERY OF THE DAY</Text>
        </View>
        <View style={styles.button}>
          <Text style={styles.mysteryText}>SORROWFUL</Text>
        </View>
      </ImageBackground>
    )
  }
}

module.exports = MysteryOfToday;