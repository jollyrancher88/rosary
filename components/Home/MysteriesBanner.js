import React, { Component } from 'react';
import { Text, View, ImageBackground } from 'react-native';
const COLORS = require ('../../lib/colors.js');

class MysteriesBanner extends Component {
  render () {
    var styles = require('./MysteriesBanner.styles.js');
    var mysteryText = this.props.mysteryText || 'GLORIOUS'
    return (
      <ImageBackground style={styles.imageBackground} source={this.props.image}>
        <View style={styles.textContainer}>
          <Text style={styles.preText}>THE</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.mysteryText}>{mysteryText}</Text>
        </View>
        <View style={styles.textContainerLower}>
          <Text style={styles.preText}>Mysteries</Text>
        </View>
      </ImageBackground>
    )
  }
}

module.exports = MysteriesBanner;