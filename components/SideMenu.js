import PropTypes from 'prop-types';
import React, {Component} from 'react';
import { Font } from 'expo';
import styles from './SideMenu.style';
import {NavigationActions} from 'react-navigation';
import {ScrollView, Text, View, Image} from 'react-native';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'open-sans-extrabold': require('../assets/fonts/OpenSans-ExtraBold.ttf'),
      'open-sans-bold': require('../assets/fonts/OpenSans-Bold.ttf'),
      'open-sans-light': require('../assets/fonts/OpenSans-Light.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  render () {
    if(this.state.fontLoaded) {
      return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
              <Text onPress={() => this.props.navigation.navigate('DrawerClose') } style={styles.sectionHeadingStyle}>
                MENU
              </Text>
              <Text onPress={() => this.props.navigation.navigate('DrawerClose') } style={styles.menuIcon}>
                &#9776;
              </Text>
            </View>
              <View style={styles.navSectionStyle}>
                <View
                  style={{
                    borderBottomColor: 'grey',
                    borderBottomWidth: 2,
                  }}
                />
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Home')}>
              HOME
              </Text>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 2,
                }}
              />
            </View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('About')}>
              ABOUT THE AFC
              </Text>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 2,
                }}
              />
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page2')}>
                JOYFUL MYSTERIES
              </Text>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 2,
                }}
              />
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page3')}>
                SORROWFUL MYSTERIES
              </Text>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 2,
                }}
              />
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page3')}>
                GLORIOUS MYSTERIES
              </Text>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 2,
                }}
              />
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page3')}>
                LUMINOUS MYSTERIES
              </Text>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 2,
                }}
              />
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerContainer}>
          <Image source={require('../assets/images/logo.png')} style={styles.footerImage} />
          <View style={styles.footerTextContainer}>
            <Text style={styles.footerText}>Apostolate for Family Consecration ®</Text>
            <Text style={styles.footerText}>3375 County Rd 36 Bloomingdale, OH 43910</Text>
            <Text style={styles.footerText}>800 77-FAMILY (773-2645)</Text>
            <Text style={styles.footerText}>info@afc.org</Text>
          </View>
        </View>
      </View>
    );
    } else {
      return(<Text>Loading...</Text>);
    }
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;