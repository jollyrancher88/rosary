import React from 'react';
import { Text, View, ImageBackground } from 'react-native';

export default class Rosary extends React.Component {
  static navigationOptions = {
      title: 'The Joyful Mysteries'
    };

  render() {
    return (
      <View>
        <Text> You Opened a Rosary!</Text>
      </View>
    );
  }
}
