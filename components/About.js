import React from 'react';
import { Text, View, ImageBackground } from 'react-native';
import { Video } from 'expo';
import Header from './Header/Header'

export default class About extends React.Component {

  render() {
    return (
      <View>
        <Header title={'ABOUT THE AFC'} navigation={this.props.navigation} />
        <Text> You Opened a About the AFC!</Text>
      </View>
    );
  }
}
