module.exports = {
  AFC_RED: '#af1f2d',

  //OLD Site Colors
  //Primary Color Palette
  EVENING_GREY: '#828598',
  DARK_GREY: '#222222',
  TRUE_WHITE: '#ffffff',
  OLD_AFC_RED: '#af1e2d',
  CLOUDS: '#ecf0f1',
  LIGHT_BLUE: '#88b5c8',

  //Secondary Color Palette
  LIGHT_GREEN: '#93c6b3',
  HAPPY_YELLOW: '#f5df67',

  //Shades and Gradients of Primary Palette
  AFC_RED_LIGHTER: '#bc2030',
  AFC_RED_DARKER: '#951a26'
};