import React from 'react';
import Expo from 'expo';
import { TouchableHighlight,
          StyleSheet,
          Text,
          View,
          ScrollView,
          ImageBackground,
          Image,
          StatusBar } from 'react-native';
import { Font } from 'expo';
import { scale } from './lib/scaling';
var MysteryOfToday = require('./components/Home/MysteryOfToday.js');
var MysteriesBanner = require('./components/Home/MysteriesBanner.js');
var ImageCard = require('./components/ImageCard/ImageCard.js');
var ButtonCard = require('./components/ButtonCard/ButtonCard.js');
var BackToTopButton = require('./components/Home/BackToTop/Button.js');

import {
  DrawerNavigator,
  StackNavigator,
  Button,
} from 'react-navigation';

import Header from './components/Header/Header'

class HomeScreen extends React.Component {
  state = {
    fontLoaded: false,
  };

  async componentDidMount() {
    await Font.loadAsync({
      'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
      'open-sans-extrabold': require('./assets/fonts/OpenSans-ExtraBold.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  onToTopButtonPress = () => {
    this.scroll.scrollTo({x: 0, y: 0, animated: true});
 }

  render() {
    var styles = require('./components/styles.js');
    if (this.state.fontLoaded) {
      return (
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',}}>
          <ScrollView ref={(c) => {this.scroll = c}} style={styles.scollView} stickyHeaderIndices={[0]} >
            <Header title={'PRAY THE ROSARY'} navigation={this.props.navigation} />
            <View style={{marginBottom: -scale(48)}}></View>
            <MysteryOfToday image={require('./assets/images/CrucifixionWithBlackOverlay.jpg')}/>
            <View style={{
                alignSelf: 'stretch',
                alignItems: 'center',
                flexDirection: 'row',
                paddingTop: 0,
                justifyContent: 'space-between'
              }}>
              <ButtonCard type={'video'} image={require('./assets/images/AgonywithBlackOverlay.jpg')} />
              <ButtonCard image={require('./assets/images/SorrowfulMarywithBlackOverlay.jpg')} />
            </View>
            <MysteriesBanner mysteryText={'GLORIOUS'} image={require('./assets/images/DecentHolySpiritBlackOverlay.jpg')}/>
            
            <View style={{
                alignSelf: 'stretch',
                alignItems: 'center',
                flexDirection: 'row',
                paddingTop: 0,
                justifyContent: 'space-between'
              }}>
              <ButtonCard type={'video'} image={require('./assets/images/AscensionwithBlackOverlay.jpg')} />
              <ButtonCard image={require('./assets/images/CorrinationwithBlackOverlay.jpg')} />
            </View>

            <MysteriesBanner mysteryText={'LUMINOUS'} image={require('./assets/images/ProclamationwithBlackOverlay.jpg')}/>
            
            <View style={{
                alignSelf: 'stretch',
                alignItems: 'center',
                flexDirection: 'row',
                paddingTop: 0,
                justifyContent: 'space-between'
              }}>
              <ButtonCard type={'video'} image={require('./assets/images/EucharistWithBlackOverlay.jpg')} />
              <ButtonCard image={require('./assets/images/WeddingCanaWithBlackOverlay.jpg')} />
            </View>

            <MysteriesBanner mysteryText={'JOYFUL'} image={require('./assets/images/AnnunciationMaryWithBlackOverlay.jpg')}/>
            
            <View style={{
                alignSelf: 'stretch',
                alignItems: 'center',
                flexDirection: 'row',
                paddingTop: 0,
                justifyContent: 'space-between'
              }}>
              <ButtonCard type={'video'} image={require('./assets/images/AnnunciationWithBlackOverlay.jpg')} />
              <ButtonCard image={require('./assets/images/VisitationWithBlackOverlay.jpg')} />
            </View>
            <BackToTopButton onPress={this.onToTopButtonPress} />
          </ScrollView>
        </View>
      )
    } else {
      return(<Text>Loading...</Text>)
    }
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
});

import About from './components/About'
import Rosary from './components/Rosary'
import SideMenu from './components/SideMenu'

// const DrawerStack = DrawerNavigator({
const RootStack = DrawerNavigator({
  Home: {
    screen: HomeScreen,
  },
  About: {
    screen: About,
  },
  Rosary: {
    screen: Rosary,
  },
 },
 {
  contentComponent: SideMenu,
  drawerWidth: 300, //380,
  navigationOptions: ({navigation}) => ({
    headerStyle: {backgroundColor: '#222222', marginTop: Expo.Constants.statusBarHeight},
    title: 'PRAY THE ROSARY',
    headerTintColor: 'white',
    mode: 'modal',
    headerLeft: <TouchableHighlight onPress={() => 
        navigation.navigate('DrawerOpen')
      }>
      <StatusBar hidden={true} />
      <Image
        source={require('./assets/images/logo.png')}
        style={{height: 30, width: 30}}
      />
    </TouchableHighlight>
  })
});

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

// const DrawerNavigation = StackNavigator({
// export default StackNavigator({
//   DrawerStack: { screen: DrawerStack }
// }, {
//   headerMode: 'float',
  
// })
